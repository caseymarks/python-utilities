import setuptools

setuptools.setup(
    name="progress-counter",
    version="0.0.1",
    author="Casey Marks",
    author_email="casey@carveacre.com",
    description="simple text progress counter",
    packages=['progress_counter'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
